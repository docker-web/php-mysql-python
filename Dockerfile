# Set up PHP server with MySQL extensions
FROM php:7-apache
RUN docker-php-ext-install mysqli pdo pdo_mysql \
    && docker-php-ext-enable pdo_mysql

# Install Python for later use of unit testing
# Install packages
RUN apt-get update
RUN apt-get install -y --no-install-recommends \
		python3 \
		python3-setuptools \
		python3-pip \
        python3-dev

# Install critical Python packages
RUN pip3 install wheel

# Default into a bash shell 
# since this will be used for development purposes
CMD ["bash"]